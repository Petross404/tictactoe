#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <time.h>

#ifdef  _WIN32
#define		CLEAR "cls"
#define		WAIT_CONSOLE_IF_WINDOWS system("pause");
#else
#define		CLEAR "clear"
#define		WAIT_CONSOLE_IF_WINDOWS
#endif

/*enum player_enum{COMPUTER = 0, PLAYER1, PLAYER2};*/

char	read_symbol_selection( char* symbol_table );

typedef struct Player
{
	int	player_;
	char	symbol_;
} Player;

typedef struct Square
{
	int	id;
	char	symbol_;
	bool	is_filled;
} Square;

typedef struct Board
{
	Square	square[3][3];
} Board;

typedef struct Board_History
{
	Board	board;
} Board_History;

void	print_skeleton( Board_History* history, int index );
void	proccess( Player* player, Board_History* history, char* symbol_table );
//void	check_if_filled(Board_History history_board);
bool	check_win( Board board );
bool	is_pos_digit( char* buf,  int len );
int	return_int_input( char* buf, Player* player,  int i );
void	fill_random_cell( int* prow,  int* pcol );
void	remove_newline( char* buf );

#endif
