#include "tictactoe.h"

int main( int argc, char* argv[] )
{
	char symbol_table[3] = {'X', 'O', '\0'};

	Player	player[2] =
	{
		{.player_ = 0, .symbol_ = 'X'},
		{.player_ = 1, .symbol_ = 'O'}
	};


	Square	square[3][3] =
	{
		{ {1, '1', false}, {2, '2', false}, {3, '3', false} },
		{ {4, '4', false}, {5, '5', false}, {6, '6', false} },
		{ {7, '7', false}, {8, '8', false}, {9, '9', false} }
	};

	Board	board;
	memcpy( board.square, square, sizeof( square ) );

	Board_History board_history[1000];

	board_history[0].board = board;
	printf( "\n\tWelcome to Tic Tac Toe\n\n" );
	print_skeleton( board_history, 0 );
	proccess( player, board_history, symbol_table );

	WAIT_CONSOLE_IF_WINDOWS

	return ( 0 );
}
