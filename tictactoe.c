#include "tictactoe.h"

void print_skeleton( Board_History* history, int index )
{
	printf( "\n\t  %c|  %c| %c\n", history[index].board.square[0][0].symbol_,
	        history[index].board.square[0][1].symbol_,
	        history[index].board.square[0][2].symbol_ );
	printf( "\t___|___|___\n" );
	printf( "\t  %c|  %c| %c\n", history[index].board.square[1][0].symbol_,
	        history[index].board.square[1][1].symbol_,
	        history[index].board.square[1][2].symbol_ );
	printf( "\t___|___|___\n" );
	printf( "\t  %c|  %c| %c\n", history[index].board.square[2][0].symbol_,
	        history[index].board.square[2][1].symbol_,
	        history[index].board.square[2][2].symbol_ );
	printf( "\t   |   |   \n" );
}

char read_symbol_selection( char* symbol_table )
{
	char symbol;
	printf( "What symbol do you want (player 1) to use? (X/O) : " );
	scanf( "%c", &symbol );

	return *strpbrk( symbol_table, &symbol );
}

bool is_pos_digit( char* buf, int len )
{
	bool is_buf_digit = true;

	char* message[] =
	{
		"Input %c isn't a valid number!\n",
		"Input %s is out of bounds!\n"
	};

	for ( int i = 0; i <= ( len - 1 ); i++ )
		if ( !isdigit( buf[i] ) && buf[i] != '\n' )
		{
			fprintf( stderr, message[0], buf[i] );
			is_buf_digit = false;
		}
		else if ( isdigit( buf[i] ) )
		{
			remove_newline( buf );

			if ( ( strtod( buf, NULL ) < 1 ) || ( strtod( buf, NULL ) > 9 ) )
			{
				fprintf( stderr, message[1], buf );
			}

			break;
		}

	return is_buf_digit;
}

int return_int_input( char* pos, Player* player, int i )
{
	int	i_pos = -1;
	int*	p_pos = NULL;

	do
	{
		printf( "What position do you choose for %c ? : ", player[i].symbol_ );
		fgets( pos, 10, stdin );

		int len = strlen( pos );

		if ( is_pos_digit( pos, len ) )
		{
			i_pos = strtod( pos, NULL );
			p_pos = &i_pos;
		}
	}
	while ( ( i_pos < 1 || i_pos > 9 ) ||
	                ( p_pos == NULL && fprintf( stderr, "Wrong input \n" ) ) );

	return i_pos;
}

void proccess( Player* player, Board_History* history, char* symbol_table )
{
	int i		= 0;			/*Player index*/
	int filled_cells = 0;			/*How many cells have been filled*/
	int h_index	= 0;			/*History index*/
	srand( time( NULL ) );			/*Run once*/
	Board l_board = history[0].board;	/*local board*/

	while ( !check_win( history[h_index].board )
	                || filled_cells < 9 )  	/*Whichever comes first*/
	{

		if ( player[i].player_ == 0 )
		{
			char	pos[] = "0";
			int	i_pos = return_int_input( pos, player, i );

			int row = i_pos / 3;
			int col = i_pos % 3;

			/*Don't overwrite cells*/
			if ( l_board.square[row][col - 1].is_filled == false )
			{
				l_board.square[row][col - 1].symbol_	= player[i].symbol_;
				l_board.square[row][col - 1].is_filled	= true;
			}
			else printf( "You can't go there!\n" );

			i++;			/*Advance to the next player*/
			filled_cells++;		/*One more cell is filled*/
		}
		else if ( player[i].player_ == 1 )
		{
			int 	row, col;
			int*	prow = &row;
			int*	pcol = &col;

			do  	/*Try until you find an empty cell to write*/
			{
				fill_random_cell( prow, pcol );
			}
			while ( l_board.square[row][col].is_filled );

			player[i].symbol_			= symbol_table[i];
			l_board.square[row][col - 1].symbol_	= symbol_table[i];
			l_board.square[row][col - 1].is_filled	= true;

			i--;			/*Go back to the previous player*/
			filled_cells++;		/*One more cell is filled again*/
		}

		history[h_index].board = l_board;
		system( CLEAR );
		print_skeleton( history, h_index );
		h_index++;
	}

	printf( "Exit from proccess\n" );
	printf( "filles cells %d, h_index %d, \n", filled_cells, h_index );
}

// void check_if_filled(Board board[][3])
// {
//
// }

bool check_win( Board board )
{
	int win_array[8][3][2] =
	{
		{ {0, 0}, {0, 1}, {0, 2} },	/*Rows*/
		{ {1, 0}, {1, 1}, {1, 2} },
		{ {2, 0}, {2, 1}, {2, 2} },
		{ {0, 0}, {1, 0}, {2, 0} },	/*Column*/
		{ {1, 0}, {1, 1}, {1, 2} },
		{ {2, 0}, {2, 1}, {2, 2} },
		{ {0, 0}, {1, 1}, {2, 2} },	/*Diagonal*/
		{ {0, 2}, {1, 1}, {2, 0} }
	};

// 	/*Check vertical lines*/
// 	for(int x = 0; x <= 3; x++)
// 	{
// 		if( (board.square[0][x].symbol_ == board.square[0][0].symbol_) &&
// 			(board.square[0][x].symbol_ == board.square[0][1].symbol_) &&
// 		(board.square[0][x].symbol_ == board.square[0][2].symbol_) )
// 		{
// 			printf("found");
// 		}
// 	}
	return false;
}

void fill_random_cell( int* prow, int* pcol )
{
	int r_pos	= rand() % 9 + 1;
	*prow		= r_pos / 3;
	*pcol		= r_pos % 3;
}

void remove_newline( char* buf )
{
	int len = strlen( buf );

	if ( buf && buf[len - 1] == '\n' )
		buf[len - 1] = '\0';
}
